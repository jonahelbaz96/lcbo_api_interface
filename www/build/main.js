webpackJsonp([0],{

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return constants; });
var constants = {
    lcboTitle: 'LCBO Product Search',
    searchPlaceholder: 'Search Products',
    quantity: 'Quan',
    stock: 'In Stock',
    closest: 'Closest 10',
    error: 'Error',
    noGeo: 'Geolocation is not supported by this browser.',
    fetchingError: 'There was an error fetching all the results. Some may be missing',
    searching: 'Search for product location',
    mapTip: '[Click for map]',
    product: 'Product'
};
//# sourceMappingURL=constants.js.map

/***/ }),

/***/ 163:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 163;

/***/ }),

/***/ 207:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/lcbo-search/lcbo-search.module": [
		208
	],
	"../pages/product-card/product-card.module": [
		302
	],
	"../pages/store-card/store-card.module": [
		301
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 207;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LcboSearchPageModule", function() { return LcboSearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lcbo_search__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_card_store_card_module__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__product_card_product_card_module__ = __webpack_require__(302);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var LcboSearchPageModule = /** @class */ (function () {
    function LcboSearchPageModule() {
    }
    LcboSearchPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__lcbo_search__["a" /* LcboSearchPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__lcbo_search__["a" /* LcboSearchPage */]),
                __WEBPACK_IMPORTED_MODULE_3__store_card_store_card_module__["StoreCardPageModule"],
                __WEBPACK_IMPORTED_MODULE_4__product_card_product_card_module__["ProductCardPageModule"]
            ],
        })
    ], LcboSearchPageModule);
    return LcboSearchPageModule;
}());

//# sourceMappingURL=lcbo-search.module.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LcboSearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_lcbo_stores_lcbo_stores__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator__ = __webpack_require__(300);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LcboSearchPage = /** @class */ (function () {
    function LcboSearchPage(navCtrl, lcbo, alertCtrl, platform, geolocation, launchNavigator, cd) {
        this.navCtrl = navCtrl;
        this.lcbo = lcbo;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.geolocation = geolocation;
        this.launchNavigator = launchNavigator;
        this.cd = cd;
        this.productSearch = '';
        this.stores = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.searchEmpty = '';
    }
    LcboSearchPage.prototype.ionViewDidLoad = function () {
        this.stores = this.lcbo.getStoreResults;
        this.waitForCompletedLoading();
        this.watchForErrors();
        this.watchForEmptyResults();
    };
    LcboSearchPage.prototype.searchProducts = function () {
        if (this.productSearch === '')
            return;
        this.isLoading = true;
        if (navigator.geolocation) {
            this.getPositionAndData();
            return;
        }
        if (this.platform.is('cordova')) {
            this.tryNativeGeolocation();
            return;
        }
        this.isLoading = false;
        this.noGeo();
    };
    LcboSearchPage.prototype.visitLocation = function (store) {
        if (this.platform.is('cordova')) {
            this.useLaunchNavigator(store);
            return;
        }
        var url = "https://maps.google.com/?q=" + store.latitude + "," + store.longitude;
        window.open(url);
    };
    LcboSearchPage.prototype.useLaunchNavigator = function (store) {
        this.launchNavigator.navigate([store.latitude, store.longitude], {})
            .then(function (success) { }, function (error) { return alert('Error launching navigator: ' + error); });
    };
    LcboSearchPage.prototype.tryNativeGeolocation = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (data) {
            _this.fetchProducts(data);
        }).catch(function (error) {
            _this.isLoading = false;
            _this.noGeo();
        });
    };
    LcboSearchPage.prototype.getPositionAndData = function () {
        var _this = this;
        navigator.geolocation.getCurrentPosition(function (data) {
            _this.fetchProducts(data);
        }, function () {
            _this.isLoading = false;
            _this.noGeo();
        });
    };
    LcboSearchPage.prototype.fetchProducts = function (position) {
        this.lcbo.resetEmpty();
        this.lcbo.getSearchedForProduct(this.productSearch, {
            lat: position.coords.latitude,
            lon: position.coords.longitude
        });
    };
    LcboSearchPage.prototype.watchForErrors = function () {
        var _this = this;
        this.lcbo.errors.subscribe(function (data) {
            if (data) {
                _this.errorFetching();
            }
        });
    };
    LcboSearchPage.prototype.watchForEmptyResults = function () {
        var _this = this;
        this.lcbo.emptyResults.subscribe(function (data) {
            _this.searchEmpty = data;
        });
    };
    LcboSearchPage.prototype.waitForCompletedLoading = function () {
        var _this = this;
        this.lcbo.doneLoading.subscribe(function () {
            _this.isLoading = false;
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
    };
    LcboSearchPage.prototype.noGeo = function () {
        this.alertCtrl.create({
            title: __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* constants */].error,
            message: __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* constants */].noGeo,
            buttons: ['OK']
        }).present();
    };
    LcboSearchPage.prototype.errorFetching = function () {
        this.alertCtrl.create({
            title: __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* constants */].error,
            message: __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* constants */].fetchingError,
            buttons: ['OK']
        }).present();
    };
    Object.defineProperty(LcboSearchPage.prototype, "product", {
        get: function () {
            return this.lcbo.searchedProduct;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LcboSearchPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* constants */];
        },
        enumerable: true,
        configurable: true
    });
    LcboSearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lcbo-search',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/pages/lcbo-search/lcbo-search.html"*/'<ion-header>\n\n  <ion-navbar text-center class="navbar-title">\n    {{constants.lcboTitle}}\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="search-empty-error">{{searchEmpty}}</div>\n  <ion-row class="search-row">\n    <ion-col col-sm-8 col-md-8 col-lg-12 class="searchbar-col">\n      <ion-input class="searchbar" [(ngModel)]="productSearch" [placeholder]="constants.searchPlaceholder" (keyup.enter)="searchProducts()"></ion-input>\n      <img src="./assets/imgs/times-white.svg" class="times-search-svg" (click)="productSearch = \'\'"/>\n    </ion-col>\n    <ion-col col-4 class="search-btn-col">\n      <button class="search-btn" (click)="searchProducts()">\n        Search\n      </button>\n    </ion-col>\n  </ion-row>\n  <ion-list class="card-list">\n    <ion-list-header *ngIf="product">{{constants.product}}</ion-list-header>\n    <ion-item no-lines no-border class="card-item even">\n      <page-product-card [product]="product"></page-product-card>\n    </ion-item>\n    <ion-list-header *ngIf="stores.getValue().length > 0">\n      <ion-row>\n        <ion-col col-6>\n          {{constants.closest}}\n        </ion-col>\n        <ion-col col-6 text-right>\n          {{constants.mapTip}}\n        </ion-col>\n      </ion-row>\n    </ion-list-header>\n    <div class="double-loader" *ngIf="isLoading">\n      <img src="./assets/imgs/double_ring.svg"/>\n    </div>\n    <ion-item no-lines no-border (click)="visitLocation(store)"\n      class="card-item" *ngFor="let store of stores | async; let i = index"\n              [ngClass]="[i % 2 === 0 ? \'even\' : \'odd\', i === 0 ? \'best-choice\' : \'\']">\n      <page-store-card [store]="store"></page-store-card>\n    </ion-item>\n  </ion-list>\n  <div class="search-products" *ngIf="stores.getValue().length === 0">{{constants.searching}}</div>\n</ion-content>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/pages/lcbo-search/lcbo-search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_lcbo_stores_lcbo_stores__["a" /* LcboStoresProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */]])
    ], LcboSearchPage);
    return LcboSearchPage;
}());

//# sourceMappingURL=lcbo-search.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LcboStoresProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_lcbo__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LcboStoresProvider = /** @class */ (function () {
    function LcboStoresProvider(http) {
        this.http = http;
        this.storeResults = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.fetchingError = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"](false);
        this.emptyResult = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        this.loadingComplete = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* EventEmitter */]();
    }
    LcboStoresProvider.prototype.getSearchedForProduct = function (searchQuery, myLocation) {
        var _this = this;
        this.getItemIds(searchQuery).subscribe(function (data) {
            var productData = data['result'];
            _this.fetchLocationForExpectedResult(productData, myLocation);
        }, function (error) {
            _this.fetchingError.next(true);
            _this.loadingComplete.emit();
        });
    };
    LcboStoresProvider.prototype.resetEmpty = function () {
        this.emptyResult.next('');
    };
    LcboStoresProvider.prototype.fetchLocationForExpectedResult = function (productData, myLocation) {
        var _this = this;
        this.storeResults.next([]);
        if (productData.length === 0) {
            this.emptyResult.next('No product found');
            this.loadingComplete.emit();
            return;
        }
        this.product = productData[0];
        this.getStoresWithProduct(String(productData[0].product_no), myLocation).subscribe(function (data) {
            _this.storeResults.next(data["result"]);
            _this.loadingComplete.emit();
            if (_this.storeResults.getValue().length === 0)
                _this.emptyResult.next('No stores found');
        }, function (error) {
            _this.fetchingError.next(true);
            _this.loadingComplete.emit();
        });
    };
    LcboStoresProvider.prototype.getItemIds = function (searchQuery) {
        var newQ = searchQuery.replace(' ', '+');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__config_lcbo__["b" /* lcbo */].baseUrl + __WEBPACK_IMPORTED_MODULE_2__config_lcbo__["b" /* lcbo */].products + '/?q=' + newQ + '&' + __WEBPACK_IMPORTED_MODULE_2__config_lcbo__["b" /* lcbo */].avoidInvalidProduct)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (data) { return data; }));
    };
    LcboStoresProvider.prototype.getStoresWithProduct = function (productId, myLocation) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__config_lcbo__["b" /* lcbo */].baseUrl + __WEBPACK_IMPORTED_MODULE_2__config_lcbo__["b" /* lcbo */].stores + '?lat=' + myLocation.lat + '&lon=' + myLocation.lon + '&product_id='
            + productId + '&' + __WEBPACK_IMPORTED_MODULE_2__config_lcbo__["b" /* lcbo */].avoidInvalidStore + '&page=1&per_page=10')
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (data) { return data; }));
    };
    Object.defineProperty(LcboStoresProvider.prototype, "getStoreResults", {
        get: function () {
            return this.storeResults;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LcboStoresProvider.prototype, "doneLoading", {
        get: function () {
            return this.loadingComplete;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LcboStoresProvider.prototype, "errors", {
        get: function () {
            return this.fetchingError;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LcboStoresProvider.prototype, "emptyResults", {
        get: function () {
            return this.emptyResult;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LcboStoresProvider.prototype, "searchedProduct", {
        get: function () {
            return this.product;
        },
        enumerable: true,
        configurable: true
    });
    LcboStoresProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */]])
    ], LcboStoresProvider);
    return LcboStoresProvider;
}());

//# sourceMappingURL=lcbo-stores.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return lcbo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return api; });
var lcbo = {
    baseUrl: 'https://lcboapi.com',
    products: '/products',
    stores: '/stores',
    avoidInvalidProduct: 'where_not=is_dead,is_discontinued',
    avoidInvalidStore: 'where_not=is_dead'
};
var api = {
    key: 'MDo3OWJkODhkNi1lYzc5LTExZTgtYWU3NS1iMzg2OGQwMWI2NDQ6SXpucjAyRnJYMTB6NVh5OXg0NG5rYTVJSE9vcHdFRk1KV3BH'
};
//# sourceMappingURL=lcbo.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoreCardPageModule", function() { return StoreCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_card__ = __webpack_require__(394);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StoreCardPageModule = /** @class */ (function () {
    function StoreCardPageModule() {
    }
    StoreCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__store_card__["a" /* StoreCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__store_card__["a" /* StoreCardPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__store_card__["a" /* StoreCardPage */]
            ]
        })
    ], StoreCardPageModule);
    return StoreCardPageModule;
}());

//# sourceMappingURL=store-card.module.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCardPageModule", function() { return ProductCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_card__ = __webpack_require__(395);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProductCardPageModule = /** @class */ (function () {
    function ProductCardPageModule() {
    }
    ProductCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__product_card__["a" /* ProductCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__product_card__["a" /* ProductCardPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__product_card__["a" /* ProductCardPage */]
            ]
        })
    ], ProductCardPageModule);
    return ProductCardPageModule;
}());

//# sourceMappingURL=product-card.module.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(357);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_lcbo_search_lcbo_search_module__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_lcbo_stores_lcbo_stores__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_request_interceptor_request_interceptor__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_geolocation__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_launch_navigator__ = __webpack_require__(300);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/lcbo-search/lcbo-search.module#LcboSearchPageModule', name: 'LcboSearchPage', segment: 'lcbo-search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-card/product-card.module#ProductCardPageModule', name: 'ProductCardPage', segment: 'product-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/store-card/store-card.module#StoreCardPageModule', name: 'StoreCardPage', segment: 'store-card', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_9__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_6__pages_lcbo_search_lcbo_search_module__["LcboSearchPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["a" /* HTTP_INTERCEPTORS */], useClass: __WEBPACK_IMPORTED_MODULE_10__providers_request_interceptor_request_interceptor__["a" /* RequestInterceptorProvider */], multi: true
                },
                __WEBPACK_IMPORTED_MODULE_7__providers_lcbo_stores_lcbo_stores__["a" /* LcboStoresProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoreCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_constants__ = __webpack_require__(130);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StoreCardPage = /** @class */ (function () {
    function StoreCardPage() {
    }
    Object.defineProperty(StoreCardPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_1__config_constants__["a" /* constants */];
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], StoreCardPage.prototype, "store", void 0);
    StoreCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-store-card',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/pages/store-card/store-card.html"*/'<ion-row>\n  <ion-col col-2 text-center>\n    <div class="quantity-title">{{constants.quantity}}</div>\n    <div class="quantity-num">{{store.quantity}}</div>\n  </ion-col>\n  <ion-col col-8 class="title-col">\n    <div class="store-title">{{store.name}}</div>\n    <div class="store-location">{{store.address_line_1}}</div>\n  </ion-col>\n  <ion-col col-2 text-center>\n    <div class="stock-title">{{constants.stock}}</div>\n    <div class="stock-num">\n      <object data="./assets/imgs/check.svg" type="image/svg+xml" *ngIf="store.quantity > 0"\n              class="check-svg">\n        <img src="./assets/imgs/check.svg" />\n      </object>\n      <object data="./assets/imgs/times.svg" type="image/svg+xml" *ngIf="store.quantity === 0"\n              class="times-svg">\n        <img src="./assets/imgs/times.svg" />\n      </object>\n    </div>\n  </ion-col>\n</ion-row>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/pages/store-card/store-card.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], StoreCardPage);
    return StoreCardPage;
}());

//# sourceMappingURL=store-card.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_constants__ = __webpack_require__(130);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductCardPage = /** @class */ (function () {
    function ProductCardPage() {
    }
    Object.defineProperty(ProductCardPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_1__config_constants__["a" /* constants */];
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ProductCardPage.prototype, "product", void 0);
    ProductCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-card',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/pages/product-card/product-card.html"*/'<ion-row *ngIf="product">\n  <ion-col col-2 text-center>\n    <img [src]="product.image_thumb_url ? product.image_thumb_url : \'./assets/imgs/alcohol.svg\'" class="product-thumbnail">\n  </ion-col>\n  <ion-col col-10 class="title-col">\n    <div class="store-title">{{product.name}}</div>\n  </ion-col>\n</ion-row>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/pages/product-card/product-card.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], ProductCardPage);
    return ProductCardPage;
}());

//# sourceMappingURL=product-card.js.map

/***/ }),

/***/ 413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_lcbo_search_lcbo_search__ = __webpack_require__(209);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_lcbo_search_lcbo_search__["a" /* LcboSearchPage */];
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/Jonah/Personal Code/lcbo_api_interface/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestInterceptorProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_operators__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_lcbo__ = __webpack_require__(214);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RequestInterceptorProvider = /** @class */ (function () {
    function RequestInterceptorProvider() {
    }
    RequestInterceptorProvider.prototype.addToken = function (req, token) {
        return req.clone({
            setHeaders: {
                Authorization: 'Token ' + token
            }
        });
    };
    RequestInterceptorProvider.prototype.intercept = function (req, next) {
        var _this = this;
        if (!navigator.onLine) {
            return __WEBPACK_IMPORTED_MODULE_0_rxjs__["Observable"].throw('No Internet Connection');
        }
        return (next.handle(this.addToken(req, __WEBPACK_IMPORTED_MODULE_3__config_lcbo__["a" /* api */].key))
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["catchError"])(function (error) {
            return _this.handleError(req, next, error);
        })));
    };
    RequestInterceptorProvider.prototype.handleError = function (req, next, error) {
        return __WEBPACK_IMPORTED_MODULE_0_rxjs__["Observable"].throw(error);
    };
    RequestInterceptorProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], RequestInterceptorProvider);
    return RequestInterceptorProvider;
}());

//# sourceMappingURL=request-interceptor.js.map

/***/ })

},[352]);
//# sourceMappingURL=main.js.map