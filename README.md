Ionic Project to produce an interactive UI for the LCBO api.

To view this project on a browser, enter the repo and run the commands:

`npm install` &
`ionic serve`

If you choose to run it on browser - for better aestethic result and UX i recommend Responsive view `iphone 6 plus`. While it is responsive to all screen sizes, it will shine brightest on a regular phone view

To view this project on iOS:
Option 1:
    - Access the project repo
    - Platforms > ios > LCBO api.xcworkspace
    - Deploy from Xcode
    
Option 2 (need ionic installed):
    - Access the project repo
    - Run `ionic cordova run ios`
        
Android version has not been built because it has not been tested. If you would like to test on Android regardless either run
`ionic cordova run android` or  `ionic cordova build android` then go to Platforms > android > app > build > outputs >apk>debug> and file transfer the file `app-debug.apk` into your android device.
