export interface StoreData {
  id: number;
  is_dead: boolean;
  name: string;
  address_line_1: string;
  quantity: number;
  store_no: number;
  latitude: number;
  longitude: number;
}
