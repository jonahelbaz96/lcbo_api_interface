export interface ProductData {
  id: number;
  is_dead: boolean;
  name: string;
  is_discontinued: string;
  product_no: number;
  image_thumb_url?: string;
}
