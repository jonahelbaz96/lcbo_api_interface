import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule, Platform} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {LcboSearchPageModule} from "../pages/lcbo-search/lcbo-search.module";
import {LcboStoresProvider} from '../providers/lcbo-stores/lcbo-stores';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RequestInterceptorProvider} from "../providers/request-interceptor/request-interceptor";
import {Geolocation} from '@ionic-native/geolocation';
import {LaunchNavigator} from "@ionic-native/launch-navigator";

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LcboSearchPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    LaunchNavigator,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {
      provide: HTTP_INTERCEPTORS, useClass: RequestInterceptorProvider, multi: true
    },
    LcboStoresProvider
  ]
})
export class AppModule {}
