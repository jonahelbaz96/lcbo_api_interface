import {HttpClient} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {lcbo} from "../../config/lcbo";
import {map} from "rxjs/operators";
import {ProductData} from "../../models/product-data";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {StoreData} from "../../models/store-data";

@Injectable()
export class LcboStoresProvider {
  private storeResults: BehaviorSubject<StoreData[]> = new BehaviorSubject<StoreData[]>([]);
  private fetchingError: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private emptyResult: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private loadingComplete: EventEmitter<any> = new EventEmitter<any>();
  private product: ProductData;
  constructor(private http: HttpClient) {}

  public getSearchedForProduct(searchQuery: string, myLocation: any): void {
    this.getItemIds(searchQuery).subscribe(data => {
      const productData: ProductData[] = data['result'];
      this.fetchLocationForExpectedResult(productData, myLocation);
    }, error => {
      this.fetchingError.next(true);
      this.loadingComplete.emit();
    });
  }

  public resetEmpty(): void {
    this.emptyResult.next('');
  }

  private fetchLocationForExpectedResult(productData: ProductData[], myLocation: any): void {
    this.storeResults.next([]);
    if (productData.length === 0) {
      this.emptyResult.next('No product found');
      this.loadingComplete.emit();
      return;
    }
    this.product = productData[0];
    this.getStoresWithProduct(String(productData[0].product_no), myLocation).subscribe(data => {
      this.storeResults.next(data["result"]);
      this.loadingComplete.emit();
      if (this.storeResults.getValue().length === 0) this.emptyResult.next('No stores found');
    }, error => {
      this.fetchingError.next(true);
      this.loadingComplete.emit();
    });
  }

  private getItemIds(searchQuery: string): Observable<any> {
    const newQ = searchQuery.replace(' ', '+');
    return this.http.get(lcbo.baseUrl + lcbo.products + '/?q=' + newQ + '&' + lcbo.avoidInvalidProduct)
      .pipe(map(data => data));
  }

  private getStoresWithProduct(productId: string, myLocation: any): Observable<any> {
    return this.http.get(lcbo.baseUrl + lcbo.stores + '?lat=' + myLocation.lat +'&lon=' + myLocation.lon +'&product_id='
      + productId + '&' + lcbo.avoidInvalidStore + '&page=1&per_page=10')
      .pipe(map(data => data));
  }

  get getStoreResults(): BehaviorSubject<StoreData[]> {
    return this.storeResults;
  }

  get doneLoading(): EventEmitter<any> {
    return this.loadingComplete;
  }

  get errors(): BehaviorSubject<boolean> {
    return this.fetchingError;
  }

  get emptyResults(): BehaviorSubject<string> {
    return this.emptyResult;
  }

  get searchedProduct(): ProductData {
    return this.product;
  }

}


