import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import {api} from "../../config/lcbo";

@Injectable()
export class RequestInterceptorProvider implements HttpInterceptor {
  constructor() { }

  private addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({
      setHeaders: {
        Authorization: 'Token ' + token
      }
    });
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<| HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    if (!navigator.onLine) {
      return Observable.throw('No Internet Connection');
    }
    return <any>
      (next.handle(this.addToken(req, api.key))
        .pipe(catchError((error: HttpErrorResponse) =>
            this.handleError(req, next, error))));
  }

  private handleError(req: HttpRequest<any>, next: HttpHandler, error: HttpErrorResponse): any {
    return Observable.throw(error);
  }
}
