import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LcboSearchPage } from './lcbo-search';
import {StoreCardPageModule} from "../store-card/store-card.module";
import {ProductCardPageModule} from "../product-card/product-card.module";

@NgModule({
  declarations: [
    LcboSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(LcboSearchPage),
    StoreCardPageModule,
    ProductCardPageModule
  ],
})
export class LcboSearchPageModule {}
