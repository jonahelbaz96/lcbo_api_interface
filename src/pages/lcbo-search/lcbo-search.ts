import {ChangeDetectorRef, Component} from '@angular/core';
import {AlertController, IonicPage, NavController, Platform} from 'ionic-angular';
import {LcboStoresProvider} from "../../providers/lcbo-stores/lcbo-stores";
import {constants} from "../../config/constants";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {StoreData} from "../../models/store-data";
import {Geolocation} from '@ionic-native/geolocation';
import {LaunchNavigator} from '@ionic-native/launch-navigator';
import {ProductData} from "../../models/product-data";

@IonicPage()
@Component({
  selector: 'page-lcbo-search',
  templateUrl: 'lcbo-search.html',
})
export class LcboSearchPage {
  public productSearch: string = '';
  public stores: BehaviorSubject<StoreData[]> = new BehaviorSubject<StoreData[]>([]);
  public isLoading: boolean;
  public searchEmpty: string = '';
  constructor(private navCtrl: NavController, private lcbo: LcboStoresProvider,
              private alertCtrl: AlertController, private platform: Platform,
              private geolocation: Geolocation, private launchNavigator: LaunchNavigator,
              private cd: ChangeDetectorRef) {
  }


  ionViewDidLoad() {
    this.stores = this.lcbo.getStoreResults;
    this.waitForCompletedLoading();
    this.watchForErrors();
    this.watchForEmptyResults();
  }

  public searchProducts(): void {
    if (this.productSearch === '') return;
    this.isLoading = true;
    if (navigator.geolocation) {
      this.getPositionAndData();
      return;
    }
    if (this.platform.is('cordova')) {
      this.tryNativeGeolocation();
      return;
    }
    this.isLoading = false;
    this.noGeo();
  }

  public visitLocation(store: StoreData): void {
    if (this.platform.is('cordova')) {
      this.useLaunchNavigator(store);
      return;
    }
    const url = "https://maps.google.com/?q=" + store.latitude + "," + store.longitude;
    window.open(url);
  }

  private useLaunchNavigator(store: StoreData): void {
    this.launchNavigator.navigate([store.latitude, store.longitude], {})
      .then(
        success => {},
        error => alert('Error launching navigator: ' + error)
      );
  }

  private tryNativeGeolocation(): void {
    this.geolocation.getCurrentPosition().then(data => {
      this.fetchProducts(data);
    }).catch( error => {
      this.isLoading = false;
      this.noGeo();
    });
  }

  private getPositionAndData(): void {
    navigator.geolocation.getCurrentPosition(data => {
      this.fetchProducts(data);
    }, () => {
      this.isLoading = false;
      this.noGeo();
    });
  }

  private fetchProducts(position: any): void {
    this.lcbo.resetEmpty();
    this.lcbo.getSearchedForProduct(this.productSearch, {
      lat: position.coords.latitude,
      lon: position.coords.longitude
    });
  }

  private watchForErrors(): void {
    this.lcbo.errors.subscribe(data => {
      if (data) {
        this.errorFetching();
      }
    });
  }

  private watchForEmptyResults(): void {
    this.lcbo.emptyResults.subscribe(data => {
      this.searchEmpty = data;
    });
  }

  private waitForCompletedLoading(): void {
    this.lcbo.doneLoading.subscribe(() => {
      this.isLoading = false;
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
  }

  private noGeo(): void {
    this.alertCtrl.create({
      title: constants.error,
      message: constants.noGeo,
      buttons: ['OK']
    }).present()
  }

  private errorFetching(): void {
    this.alertCtrl.create({
      title: constants.error,
      message: constants.fetchingError,
      buttons: ['OK']
    }).present()
  }

  get product(): ProductData {
    return this.lcbo.searchedProduct;
  }

  get constants(): any {
    return constants;
  }
}
