import {Component, Input} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {ProductData} from "../../models/product-data";
import {constants} from "../../config/constants";

@IonicPage()
@Component({
  selector: 'page-product-card',
  templateUrl: 'product-card.html',
})
export class ProductCardPage {
  @Input() product: ProductData;
  constructor() {
  }

  get constants(): any {
    return constants;
  }
}
