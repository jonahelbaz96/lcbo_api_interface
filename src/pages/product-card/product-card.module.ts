import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductCardPage } from './product-card';

@NgModule({
  declarations: [
    ProductCardPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductCardPage),
  ],
  exports: [
    ProductCardPage
  ]
})
export class ProductCardPageModule {}
