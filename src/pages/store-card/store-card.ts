import {Component, Input} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {StoreData} from "../../models/store-data";
import {constants} from "../../config/constants";

@IonicPage()
@Component({
  selector: 'page-store-card',
  templateUrl: 'store-card.html',
})
export class StoreCardPage {
  @Input() store: StoreData;
  constructor() {
  }

  get constants(): any {
    return constants;
  }
}
