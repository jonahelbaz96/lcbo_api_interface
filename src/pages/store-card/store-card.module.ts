import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoreCardPage } from './store-card';

@NgModule({
  declarations: [
    StoreCardPage,
  ],
  imports: [
    IonicPageModule.forChild(StoreCardPage),
  ],
  exports: [
    StoreCardPage
  ]
})
export class StoreCardPageModule {}
