export const lcbo = {
  baseUrl: 'https://lcboapi.com',
  products: '/products',
  stores: '/stores',
  avoidInvalidProduct: 'where_not=is_dead,is_discontinued',
  avoidInvalidStore: 'where_not=is_dead'
};

export const api = {
  key: 'MDo3OWJkODhkNi1lYzc5LTExZTgtYWU3NS1iMzg2OGQwMWI2NDQ6SXpucjAyRnJYMTB6NVh5OXg0NG5rYTVJSE9vcHdFRk1KV3BH'
};
