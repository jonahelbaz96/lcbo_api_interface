export const constants = {
  lcboTitle: 'LCBO Product Search',
  searchPlaceholder: 'Search Products',
  quantity: 'Quan',
  stock: 'In Stock',
  closest: 'Closest 10',
  error: 'Error',
  noGeo: 'Geolocation is not supported by this browser.',
  fetchingError: 'There was an error fetching all the results. Some may be missing',
  searching: 'Search for product location',
  mapTip: '[Click for map]',
  product: 'Product'
};
